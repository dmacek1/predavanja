--1. primjer, dohvaćanje rowida
DROP SEQUENCE seq_prodaja_id;
DROP table mobiteli;
select rowid, ime, prezime from studenti;
select rowid, s.* from studenti s where rowid = 'AAA0qwAAHAAACndAAA';

--2.primjer, dohvaćanje podataka 
select * from korisnici;
select * from prodaja;

--3. primjer, kreiranje stranog ključa
ALTER TABLE prodaja
ADD CONSTRAINT fk_korisnici
   FOREIGN KEY (IDkorisnika)
   REFERENCES korisnici  (ID);
   
--4. primjer, kreiranje primarnog ključa
ALTER TABLE korisnici
ADD CONSTRAINT pk_korisnici
   PRIMARY KEY (ID);

ALTER TABLE prodaja
ADD CONSTRAINT fk_korisnici
   FOREIGN KEY (IDkorisnika)
   REFERENCES korisnici  (ID);
   
--5. primjer, kreiranje sequence i unos podataka u tablicu prodaja
select * from prodaja;
CREATE SEQUENCE seq_prodaja_id
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 78432
  INCREMENT BY 1
  CACHE 20;
  
insert into prodaja 
      (ID, IDkorisnika, cijena, porez, dostava)
   values
      (seq_prodaja_id.nextval, 12345, 400, 100, 18 );
      
--6. primjer
select * from korisnici;
insert into korisnici
   (ID, ime, prezime, email )
values
   (12345, 'Ivica', 'Molnar', 'imolnar@gmail.com');
   
--7. primjer, ubacivanje podataka u tablicu prodaja
insert into prodaja 
      (ID, IDkorisnika, cijena, porez, dostava)
   values
      (seq_prodaja_id.nextval, 12345, 400, 100, 18 );
select * from prodaja;
insert into prodaja 
      (ID, IDkorisnika, cijena, porez, dostava)
   values
      (seq_prodaja_id.nextval, 73298, 256, 20, 11 );
select * from prodaja;

--8. primjer, brisanje podataka iz povezanih tablica
delete from korisnici where ID = 12345;
select * from prodaja where idkorisnika = 12345;
delete from prodaja where id = 78492;
delete from korisnici where ID = 12345;
select * from korisnici;
select * from prodaja;

--9.primjer, izmjena stranog ključa
ALTER TABLE PRODAJA
DROP CONSTRAINT fk_korisnici; 

ALTER TABLE prodaja
ADD CONSTRAINT fk_korisnici
   FOREIGN KEY (IDkorisnika)
   REFERENCES korisnici  (ID)
   ON DELETE CASCADE;
   
select * from korisnici;
insert into korisnici
   (ID, ime, prezime, email )
values
   (SEQ_KORISNICI_ID.nextval, 'Maja', 'Celing', 'mceling@gmail.com');
select * from korisnici;
select * from prodaja;

insert into prodaja 
      (ID, IDkorisnika, cijena, porez, dostava)
   values
      (seq_prodaja_id.nextval, 73299, 139, 24, 6 );
      
insert into prodaja 
      (ID, IDkorisnika, cijena, porez, dostava)
   values
      (seq_prodaja_id.nextval, 73299, 756, 100, 15 );

select * from prodaja;
delete from korisnici where id = 73299;
select * from prodaja;

--10. primjer, želim promijeniti ID korisnika s 73298 na 73299
update korisnici set id = 73299 where id = 73298;
ALTER TABLE prodaja
DISABLE CONSTRAINT fk_korisnici;
update korisnici set id = 73299 where id = 73298;
update prodaja set idkorisnika = 73299 where idkorisnika = 73298;
ALTER TABLE prodaja
ENABLE CONSTRAINT fk_korisnici;
SELECT * FROM PRODAJA;

--11. primjer, check constraint na pozivne brojeve mobitela
CREATE TABLE mobiteli(
  ID numeric(8),  
  pozivni numeric(3),
  mobitel numeric(7),
  CONSTRAINT ck_pozivni
  CHECK (pozivni in(99,98,97,95,92,91)));
  
insert into mobiteli (id, pozivni, mobitel) values (1, 99, 5982354);
select * from mobiteli;
insert into mobiteli (id, pozivni, mobitel) values (1, 96, 1234567);

--12. primjer, kako dohvaćati podatke iz tablica
insert into korisnici
   (ID, ime, prezime, email )
values
   (SEQ_KORISNICI_ID.nextval, 'Maja', 'Celing', 'mceling@gmail.com');

insert into prodaja 
      (ID, IDkorisnika, cijena, porez, dostava)
   values
      (seq_prodaja_id.nextval, 73318, 139, 24, 6 );
      
insert into prodaja 
      (ID, IDkorisnika, cijena, porez, dostava)
   values
      (seq_prodaja_id.nextval, 73318, 756, 100, 15 );   
   
select * from korisnici;
select * from prodaja;

select * from korisnici k, prodaja p where k.id = p.idkorisnika;
--kartezijev produkt, vrlo zahtjevna i uglavnom nepotrebna operacija
select * from korisnici, prodaja;
select * from prodaja, korisnici;












   




  



